use crate::api::structs::{Collection, Room, Zone};
use crate::Settings;
use reqwest::header;
use reqwest::StatusCode;
use serde::de::DeserializeOwned;
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub enum HTTPError {
    ErrorResponse {
        reason: String,
        status: reqwest::StatusCode,
    },
    Client(reqwest::Error),
}

impl fmt::Display for HTTPError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "HTTPError is here!")
    }
}

impl From<reqwest::Error> for HTTPError {
    fn from(error: reqwest::Error) -> HTTPError {
        HTTPError::Client(error)
    }
}

impl Error for HTTPError {}

pub struct Client {
    reqwest_client: reqwest::blocking::Client,
    base_url: String,
}

impl Client {
    pub fn new(config: &Settings) -> Result<Client, reqwest::Error> {
        let mut headers = header::HeaderMap::new();
        headers.insert(
            header::AUTHORIZATION,
            format!("Bearer {}", config.api_token()).parse().unwrap(),
        );

        match reqwest::blocking::Client::builder()
            .default_headers(headers)
            .build()
        {
            Ok(reqwest_client) => Ok(Client {
                base_url: String::from(config.base_url()),
                reqwest_client,
            }),

            Err(e) => Err(e),
        }
    }

    pub fn room(&self, room_id: &str) -> Result<Room, HTTPError> {
        self.get(format!("{}/api/rooms/{}", self.base_url, room_id))
    }

    pub fn rooms(&self, href: &str) -> Result<Collection<Room>, HTTPError> {
        self.get(format!("{}{}", self.base_url, href))
    }

    pub fn zone(&self, zone_id: &str) -> Result<Zone, HTTPError> {
        self.get(format!("{}/api/zones/{}", self.base_url, zone_id))
    }

    pub fn zones(&self) -> Result<Collection<Zone>, HTTPError> {
        self.get(format!("{}/api/zones", self.base_url))
    }

    fn get<T: DeserializeOwned>(&self, url: String) -> Result<T, HTTPError> {
        let response = self.reqwest_client.get(&url).send()?;

        match response.status() {
            StatusCode::OK => Ok(response.json()?),
            s => Err(HTTPError::ErrorResponse {
                status: s,
                reason: String::from("Non success status code"),
            }),
        }
    }
}
