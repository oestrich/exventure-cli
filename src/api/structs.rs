use ansi_term::Colour;
use serde::Deserialize;
use std::fmt;

#[derive(Deserialize, Debug)]
pub struct Link {
    pub href: String,
    pub rel: String,
}

impl Link {
    pub fn find(links: &[Link], rel: String) -> Option<&Link> {
        links.iter().find(|&link| link.rel == rel)
    }
}

#[derive(Deserialize, Debug)]
pub struct Room {
    pub name: String,
    pub description: String,
    #[serde(alias = "live?")]
    is_live: bool,
    links: Vec<Link>,
}

impl fmt::Display for Room {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", Colour::Blue.paint(&self.name))
    }
}

#[derive(Deserialize, Debug)]
pub struct Zone {
    pub name: String,
    #[serde(alias = "live?")]
    is_live: bool,
    pub links: Vec<Link>,
}

impl fmt::Display for Zone {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", Colour::Yellow.paint(&self.name))
    }
}

#[derive(Deserialize, Debug)]
pub struct Collection<T> {
    pub items: Vec<T>,
    links: Vec<Link>,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_room_format() {
        let room = Room {
            name: String::from("Room Name"),
            description: String::from("A room"),
            is_live: true,
            links: Vec::new(),
        };

        assert_eq!(format!("{}", room), "\u{1b}[34mRoom Name\u{1b}[0m");
    }

    #[test]
    fn test_zone_format() {
        let zone = Zone {
            name: String::from("Zone Name"),
            is_live: true,
            links: Vec::new(),
        };

        assert_eq!(format!("{}", zone), "\u{1b}[33mZone Name\u{1b}[0m");
    }
}
