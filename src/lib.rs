use crate::api::{Client, Link};
use clap::ArgMatches;
use std::error::Error;
use std::fmt;

pub mod api;
mod settings;

pub use settings::Settings;

#[derive(Debug)]
pub enum ArgError {
    Missing(String),
}

impl fmt::Display for ArgError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "ArgError is here!")
    }
}

impl Error for ArgError {}

/// Command for fetching a resource from the server
pub fn command_get(settings: &Settings, matches: &ArgMatches) -> Result<(), Box<dyn Error>> {
    let resource = matches
        .value_of("resource")
        .unwrap()
        .split('/')
        .collect::<Vec<&str>>();

    match resource[..] {
        ["rooms"] => get_rooms(&settings),
        ["rooms", ""] => Err(Box::new(ArgError::Missing(String::from("id")))),
        ["rooms", id] => get_room(&settings, id),
        ["zones"] => get_zones(&settings),
        ["zones", ""] => Err(Box::new(ArgError::Missing(String::from("id")))),
        ["zones", id] => get_zone(&settings, id),
        _ => Ok(()),
    }
}

/// Get all rooms and display them
pub fn get_rooms(settings: &Settings) -> Result<(), Box<dyn Error>> {
    let client = Client::new(settings)?;

    let rooms = client.rooms("/api/rooms")?;

    println!("Rooms:");

    for room in &rooms.items {
        println!("- {}", room);
    }

    Ok(())
}

/// Get a Room and display it
pub fn get_room(settings: &Settings, id: &str) -> Result<(), Box<dyn Error>> {
    let client = Client::new(settings)?;

    let room = match client.room(id) {
        Ok(room) => room,
        Err(err) => {
            eprintln!("Couldn't load room {}", id);
            return Err(Box::new(err));
        }
    };

    println!("{}", room);

    Ok(())
}

pub fn get_zones(settings: &Settings) -> Result<(), Box<dyn Error>> {
    let client = Client::new(settings)?;

    let zones = client.zones()?;

    println!("Zones:");

    for zone in &zones.items {
        println!("- {}", zone);
    }

    Ok(())
}

/// Get a zone and display it
pub fn get_zone(settings: &Settings, id: &str) -> Result<(), Box<dyn Error>> {
    let client = Client::new(settings)?;

    let zone = match client.zone(id) {
        Ok(zone) => zone,
        Err(err) => {
            eprintln!("Couldn't load zone {}", id);
            return Err(Box::new(err));
        }
    };

    println!("{}", zone);

    if let Some(link) = Link::find(&zone.links, String::from("rooms")) {
        let rooms = client.rooms(&link.href)?;

        for room in rooms.items {
            println!("\t- {}", room);
        }
    }

    Ok(())
}
