use clap::{App, AppSettings, Arg, ArgMatches};
use exventure::command_get;
use exventure::Settings;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("ExVenture CLI")
        .version("1.0")
        .author("Eric O. <eric@oestrich.org>")
        .about("Command line interface for an ExVenture instance")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .arg(
            Arg::new("config")
                .short('C')
                .long("config")
                .value_name("FILE")
                .about("Sets a custom config file")
                .default_value("config.toml")
                .takes_value(true),
        )
        .arg(
            Arg::new("url")
                .long("url")
                .value_name("URL")
                .about("Override the base URL")
                .takes_value(true),
        )
        .arg(
            Arg::new("token")
                .short('t')
                .long("token")
                .value_name("API_TOKEN")
                .about("Override the API token")
                .takes_value(true),
        )
        .subcommand(App::new("get").arg(Arg::new("resource").takes_value(true).required(true)))
        .subcommand(
            App::new("zones")
                .setting(AppSettings::SubcommandRequiredElseHelp)
                .subcommand(App::new("get").arg(Arg::new("id").takes_value(true).required(true))),
        )
        .get_matches();

    let config = matches.value_of("config").unwrap();

    let mut settings = Settings::new(config)?;
    let settings = settings
        .with_base_url(matches.value_of("url"))
        .with_api_token(matches.value_of("token"));

    std::process::exit(match subcommands(matches, &settings) {
        Ok(_) => 0,
        Err(err) => {
            eprintln!("{:?}", err);
            1
        }
    });
}

fn subcommands(matches: ArgMatches, settings: &Settings) -> Result<(), Box<dyn Error>> {
    match matches.subcommand() {
        Some(("get", matches)) => command_get(&settings, matches),
        _ => unreachable!(),
    }
}
