use config::{Config, ConfigError, File};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Settings {
    api_token: String,
    base_url: String,
}

impl Settings {
    pub fn new(file: &str) -> Result<Self, ConfigError> {
        let mut settings = Config::new();
        settings.merge(File::with_name(file))?;
        settings.try_into()
    }

    pub fn api_token(&self) -> &str {
        &self.api_token
    }

    pub fn with_api_token(&mut self, api_token: Option<&str>) -> &mut Self {
        match api_token {
            Some(api_token) => {
                self.api_token = String::from(api_token);
                self
            }
            None => self,
        }
    }

    pub fn base_url(&self) -> &str {
        &self.base_url
    }

    pub fn with_base_url(&mut self, base_url: Option<&str>) -> &mut Self {
        match base_url {
            Some(base_url) => {
                self.base_url = String::from(base_url);
                self
            }
            None => self,
        }
    }
}
