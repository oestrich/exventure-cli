#!/bin/bash

set -ex

cargo fmt -- --check
cargo clippy -- -D warnings
cargo test
